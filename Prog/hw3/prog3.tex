\documentclass[12pt]{exam}
\usepackage{amsmath}
\usepackage[left=1in, right=1in, top=1in, bottom=1in]{geometry}
\usepackage{graphicx}
\usepackage{color}

\newcommand\Cnought{C$_0$}
\newcommand\modulo{\ \texttt{\%}\ }
\newcommand\lshift{\ \texttt{<<}\ }
\newcommand\rshift{\ \texttt{>>}\ }
\newcommand\cgeq{\ \texttt{>=}\ }
\newtheorem{task}{Task}
\newtheorem{ectask}{Extra Credit Task}
\newtheorem{exercise}{Exercise}

\newcommand{\answerbox}[1]{
\begin{framed}
\hspace{5.65in}
\vspace{#1}
\end{framed}}


\pagestyle{head}

\headrule \header{\textbf{15-122 Assignment 3}}{}{\textbf{Page
\thepage\ of \numpages}}

\pointsinmargin \printanswers

\setlength\answerlinelength{2in} \setlength\answerskip{0.3in}

\begin{document}

\begin{center}
\textbf{\large{15-122 : Principles of Imperative Computation
 \\ \vspace{0.2in} Spring 2012
\\  \vspace{0.2in} Assignment 3: Word Ladders
}}

 \vspace{0.2in}
 (\large{Programming Part})

 \vspace{0.2in}

 \large{Due: Tuesday, February 23, 2012 by 23:59 }
\end{center}


\vspace{0.5in}

\noindent
For the programming portion of this week's homework, you'll write two
\Cnought{} files corresponding to different path search tasks:
    \begin{itemize}
\item \texttt{path-util.c0} (described in Section~\ref{sect:util}),
\item \texttt{path-search.c0} (described in Section~\ref{sect:search})

\end{itemize}

\vspace{0.1in}
\noindent
You should submit these two files electronically by the due date.
Detailed submission instructions can be found below.

% End title box


\newpage


\section{\textbf{\large{Assignment: Word Ladders (25 points)}}}


\paragraph{Starter code.}  Download the file \texttt{hw3.zip} from
the course website.  When you unzip it, you will find the following files

\begin{center}
\begin{tabular}{ll}

\texttt{{queue.c0}}  &   Queue containing stacks\\
\texttt{{stack.c0}}  &   Stacks containing string (paths) \\
\texttt{{readfile.c0}} &  Code for reading words from a file \\
\texttt{{path-main.c0}} &  The driver program for testing \\
\texttt{{dictionary.txt}}  &   Sorted dictionary\\
\\
 \texttt{{path-util.c0}} &  Utility functions for accessing path information\\
\texttt{{path-search.c0}} & Search functions for finding path information\\



\end{tabular}
\end{center}
You will also see a \texttt{tests/} directory with some sample
text files you may use to test your code.

\paragraph{Compiling and running.}  For this homework, use the \texttt{cc0}
command as usual to compile your code.
Don't forget to test your annotations by compiling with the \texttt{-d}
switch to enable dynamic checking.

\paragraph{Submitting.} Once you have completed some files, you can submit them
to autolab under the Homework 3 handin link as for previous assignments.  
To do so, please tar them, for example: \begin{center} \texttt{tar -cvfz
hw2\_solutions.tgz path\_util.c0 path\_search.c0} \end{center} Then go to
https://autolab.cs.cmu.edu/15122-s12 and submit the tar as your
solutions to homework 2.  You can submit as many times as you like before
the deadline.  When we grade your assignment, we will only consider the
most recent submission. 

\paragraph{Annotations.} Be sure to include appropriate \texttt{//@requires},
\texttt{//@ensures}, \texttt{//@assert}, and \texttt{//@loop\_invariant}
annotations in your program. You should write these as you are writing the
code rather than after you're done: documenting your code as you go along
will help you reason about what it should be doing, and thus help you write
code that is both clearer and more correct.  A portion of your score will be allocated to good annotations.

\paragraph{Style.}
Strive to write code with \textit{good style}: indent every line of a block
to the same level, use descriptive variable names, keep lines to 80
characters or fewer, document your code with comments, etc.  We will read
your code when we grade it, and a portion of your score will be allocated to good style.
Feel free to ask on the course bboard (\texttt{academic.cs.15-122}) if
you're unsure of what constitutes good style.

\paragraph{Task 0 (8 points)}
8 points on this assignment will be awarded based on annotations and style, so make sure to follow the guidelines given above.


\subsection{Word Ladders Overview}

Word ladders were invented by Lewis Carroll in 1878, the author of \textit{Alice in Wonderland}. A ladder is a sequence of words that starts at a starting word, and ends at an ending word, and contains a sequence of words in between where each word differs by one letter from the word before it. Put another way, in a word ladder you have to change one word into another by altering a single letter at each step. Each word in the ladder must be a valid English word, and must have the same length. For example, to turn \texttt{stone} into \texttt{money}, one possible ladder is given below. Note that the letter that was changed is shown in the color red.

\vspace{0.1in}
\noindent
stone $\rightarrow$
{\color{red}a}tone $\rightarrow$
a{\color{red}l}one $\rightarrow$
{\color{red}c}lone $\rightarrow$
clon{\color{red}s} $\rightarrow$
c{\color{red}o}ons $\rightarrow$
co{\color{red}n}ns $\rightarrow$
con{\color{red}e}s $\rightarrow$
cone{\color{red}y} $\rightarrow$
{\color{red}m}oney
\vspace{0.1in}

\noindent
Many word ladders have more than one possible solution, and there could be more than one shortest solution. For example another path from \texttt{stone} to \texttt{money} is:

\vspace{0.1in}
\texttt{{\color{blue}stone  store  shore  chore  choke  choky  cooky  cooey  coney  money}}
\vspace{0.1in}

\noindent
Your program must determine a \textit{shortest} word ladder for a set of starting and ending words.\\*

\subsection{Instructions}

%Your program will accept a file that contains starting and ending words from the input file called ``tests/easy.txt''. Each line contains two words where the first word in each line is the starter word and the second word is the target word. You may assume that the file is valid. That is, each line contains two words of the same length You use \texttt{readfile.c0} to process this file and break start and target words appropriately.

\texttt{path-main.c0} (provided) will accept a file that contains starting and
ending words from the input file called ``tests/easy.txt''.  (You may wish to
modify this in order to test other files.)  Each line contains two words
where the first word in each line is the starter word and the second word is
the target word.  This program will call functions provided by your code.  You
can compile it using the command \begin{center} \texttt{cc0 [-d] path-main.c0} \end{center} There is no need to include multiple files on the command line, since we are
using \#use statements within each file.  You should only include the -d flag when testing on small inputs.  Use this program to test your code by comparing the output of your program to the output provided in ``tests/easy.out''. 

Note that \texttt{string\_bundle} code is provided in \texttt{readfile.c0} as in 
the previous assignment.  You may find that the interface which was
sufficient for Assignment 2 does not provide all the functionality required
for Assignment 3.  How you solve this problem is up to you.  While you
are solving it, you may wish to consider both the difficulty and
importance of good interface design.

\subsection{Suggested Algorithm.}  There are several ways to solve this problem. One method involves using a queue of stacks to find word ladders. The algorithm consists of a initialization step and a while loop:

\textbf{Initialization:}
Get the starting word, push it on a empty stack, and then enqueue this stack to a queue. Note, the queue contains only one stack, so far.

\textbf{Loop:}
Dequeue a stack from the queue and then take it's top string (let us call it \texttt{top\_str}.)  If the target word is equal to \texttt{top\_str} then you are done - return the stack you popped from the queue.
Next, you search through the dictionary to find all words that differ by one letter from \texttt{top\_str}. Let's call this set \texttt{new\_words}. Create a new stack (use the \texttt{clone()} function from \texttt{stack.c0}) for each of the words in \texttt{new\_words}. Each stack must be a \textbf{deep} copy of the dequeued stack. Push on it a word from the set \texttt{new\_words}. Enqueue all these stacks into the queue.

You continue this process until a word path is found or the queue is empty. A graphical representation of this process is given in the figure below. You are allowed to use slight variations of this algorithm, but be sure to comply with the function prototypes provided.

\textit{{\color{red}Caution:}} you have to keep track of the used words! Otherwise an infinite loop can occur. With a minor tweak to the code that processes the dictionary of equal length words, you can make this happen.


\subsection{Example.}  The starting word is ``smart''. Find all the words one letter different from ``smart'', push them into different stacks and store stacks in the queue. This table represents a queue of stacks.
\begin{verbatim}
-----------------------------------------
| scart | start | swart | smalt | smarm |
| smart | smart | smart | smart | smart |
-----------------------------------------
\end{verbatim}
Now dequeue the front ``scart-smart'' and find all words one letter different from the top word which is ``scart''. This will spawn seven new stacks, which we enqueue to the queue. The queue size now is 11.


\begin{verbatim}
-----------------------------------------------------------------------------
                            |scant |scatt |scare |scarf |scarp |scars |scary|
|start |swart |smalt |smarm |scart |scart |scart |scart |scart |scart |scart|
|smart |smart |smart |smart |smart |smart |smart |smart |smart |smart |smart|
-----------------------------------------------------------------------------
\end{verbatim}
Again dequeue the front ``start-smart'' and find all words one letter different from the top word ``start''. This will spawn four new stacks:
\begin{verbatim}
---------------------------------
| sturt | stare | stark | stars |
| start | start | start | start |
| smart | smart | smart | smart |
---------------------------------
\end{verbatim}
Add them to the queue. The queue size now is 14. Repeat the procedure until either you find the ending word or you determine that the word ladder does not exist. Make sure you do not run into an infinite loop!

As you can imagine there is quite a bit of memory-hogging here. Still your implementation must terminate in a reasonable time.



\newpage
\subsection{Required: Completing path-util}
\label{sect:util}
The purpose of this section is to develop the utility functions that you may need in the other parts of the program as well as to use in the annotations. You must maintain the following function prototypes. You can add more functions to be used in annotations.


\begin{task}[2 pts]
Complete the function \texttt{word\_distance} as specified below:
\begin{verbatim}
   int word_distance(string s1, string s2)
\end{verbatim}
The purpose of this function is to find the word distance between two given words of equal length. The function will find the number of places where two words differ. For example, ``cat'' and ``bat'' will return 1 and ``frank'' and ``pranc'' will return 2.
\end{task}

\begin{task}[2 pts]
Complete the function \texttt{count\_words} as specified below:
\begin{verbatim}
   int count_words(string_bundle dictionary, int wordLength)
\end{verbatim}
The purpose of this function is to count words in the dictionary that are of length \texttt{wordLength}. The \texttt{string\_bundle} structure is defined in \texttt{readfile.c0}.
\end{task}

\begin{task}[2 pts]
Complete the function \texttt{check\_dictionary} as specified below:
\begin{verbatim}
   bool check_dictionary(string_bundle dictionary, int wordLength)
\end{verbatim}
The purpose of this function is to confirm (and return true) that all words in the dictionary are of length \texttt{wordLength}. The function returns false otherwise.
\end{task}

\begin{task}[2 pts]
Complete the function \texttt{contains} as specified below:
\begin{verbatim}
   bool contains(string str, string_bundle dictionary)
\end{verbatim}
The purpose of this function is to test whether the dictionary contains a given string.

\end{task}




\subsection{Required: Completing path-search}
\label{sect:search}
This file should contain the functions required to successfully find (or not) a path from start to target word. You must maintain the following function prototypes. You may add other helper functions. Be sure to add annotations for all of your code!

\begin{task}[3 pts]
Complete the function \texttt{words\_same\_length} as specified below:
\begin{verbatim}
   string_bundle words_same_length(string_bundle dictionary, int wordLength);
\end{verbatim}
The purpose of this function is to find all words from the dictionary of length equal to  \texttt{wordLength}. This function will return a smaller dictionary with all words of the same length. The function \texttt{find\_path} will use this smaller dictionary for finding a path between two given words.
\end{task}

\begin{task}[6 pts]
Complete the function \texttt{find\_path} as specified below:
\begin{verbatim}
   stack find_path(string start, string end, string_bundle dictionary)
\end{verbatim}
The function takes a dictionary and returns a stack that contains the path from start to end. It returns an empty stack when no path is found.
\end{task}





\subsection{Honors Version (No extra points)}
\label{sect:honors}
We define an \textit{island} as a word that has no path to any other words in the dictionary. In other words, the function \texttt{find\_path()} called on an island and any other word will always return an empty stack. Your task is to complete the function
\begin{verbatim}
   int islands(string_bundle dictionary, int wordLength)
\end{verbatim}
that returns the number of islands of the fixed length \texttt{wordLength}. For example, there are 13449 islands of length 8. You don't need to submit your solution for this task. Good luck.

\end{document}
