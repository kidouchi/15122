#include <assert.h>
#include <limits.h>
#include <stdlib.h>
#include <signal.h>

#include "xalloc.h"
#include "contracts.h"
#include "stacks.h"

#include "bare.h"
#include "c0vm.h"
#include "c0vm_c0ffi.h"


/* call stack frames */
typedef struct frame * frame;
struct frame {
    c0_value *V; /* local variables */
    stack S;     /* operand stack */
    ubyte *P;    /* function body */
    int pc;      /* return address */
};


/* functions for handling errors */
void c0_memory_error(char *err) {
    fprintf(stderr, "Memory error: %s\n", err);
    raise(SIGUSR1);
}

void c0_division_error(char *err) {
    fprintf(stderr, "Division error: %s\n", err);
    raise(SIGUSR2);
}


/* TODO: implement execute function */
int execute(struct bc0_file *bc0) {
    /* Variables used for bytecode interpreter. You will need to initialize
       these appropriately. */

    /* callStack to hold frames when functions are called */
    stack callStack = stack_new();


    /* initial program is the "main" function, function 0 (which must exist) */
    struct function_info *main_fn = xmalloc(sizeof(struct function_info));
    main_fn->num_args = 0; //since main fn
    main_fn->num_vars = bc0->function_pool->num_vars;
    main_fn->code_length = bc0->function_pool->code_length;
    main_fn->code = bc0->function_pool->code;

    /* array to hold local variables for function */
    c0_value *V = xcalloc(bc0->function_pool->num_vars,sizeof(c0_value));

    /* stack for operands for computations */
    stack S = stack_new();
    /* array of (unsigned) bytes that make up the program */
    ubyte *P = bc0->function_pool->code;
    /* program counter that holds "address" of next bytecode to interpret from
       program P */
    int pc = 0;

    while (true) {

#ifdef DEBUG
      printf("Executing opcode %x  --- Operand stack size: %d\n",
             P[pc], stack_size(S));
#endif

        switch (P[pc]) {

        /* GENERAL INSTRUCTIONS: Implement the following cases for each of the
           possible bytecodes.  Read the instructions in the assignment to see
           how far you should go before you test your code at each stage.  Do
           not try to write all of the code below at once!  Remember to update
           the program counter (pc) for each case, depending on the number of
           bytes needed for each bytecode operation.  See PROG_HINTS.txt for
           a few more hints.

           IMPORTANT NOTE: For each case, the case should end with a break
           statement to prevent the execution from continuing on into the
           next case.  See the POP case for an example.  To introduce new
           local variables in a case, use curly braces to create a new block.
           See the DUP case for an example.

           See C_IDIOMS.txt for further information on idioms you may find
           useful.
        */

            /* Additional stack operation: */

            //pop out 
   	        case POP: 
            {
                REQUIRES(!stack_empty(S));
                pop(S);
                pc++;
                break;
            }

            //duplicate 
            case DUP: 
            {
                REQUIRES(!stack_empty(S));
                c0_value v = pop(S);
                push(S, v);
                push(S, v);
                pc++;
                ENSURES(!stack_empty(S));
                break;
            }

            //swap
            case SWAP:
            {
                REQUIRES(!stack_empty(S));
                c0_value v1 = pop(S);
                c0_value v2 = pop(S);
                push(S, v1);
                push(S, v2);
                pc++;
                ENSURES(!stack_empty(S));
                break;
            }



            /* Arithmetic and Logical operations */

            //add x + y
            case IADD:
            {
                REQUIRES(!stack_empty(S));
                c0_value y = pop(S);
                c0_value x = pop(S);
                int vadd = INT(x) + INT(y);
                push(S, VAL(vadd));
                pc++;
                ENSURES(!stack_empty(S));
                break;
            }

            //subtract x - y
            case ISUB:
            {   
                REQUIRES(!stack_empty(S));
                c0_value y = pop(S);
                c0_value x = pop(S);
                int vsub = INT(x) - INT(y);
                push(S, VAL(vsub));
                pc++;
                ENSURES(!stack_empty(S));
                break;
            }

            //multiply x * y
            case IMUL:
            {
                REQUIRES(!stack_empty(S));
                c0_value y = pop(S);
                c0_value x = pop(S);
                int vmul = INT(x) * INT(y);
                push(S, VAL(vmul));
                pc++;
                ENSURES(!stack_empty(S));
                break;
            }
            
            //divide x/y
            case IDIV:
            {
                REQUIRES(!stack_empty(S));
                c0_value y = pop(S);
                
                //takes care of zero division error case
                if (INT(y) == 0)
                {
                    c0_division_error("DIVISION BY 0 ERROR");
                    abort();
                }
                c0_value x = pop(S);
                //case of division error 
                if (INT(x) == INT_MIN && INT(y) == -1)
                {
                    c0_division_error("Div Error");
                    abort();
                }
                int vdiv = INT(x) / INT(y);
                push(S, VAL(vdiv));
                pc++;
                ENSURES(!stack_empty(S));
                break;
            }

            //modular arith x % y
            case IREM:
            {
                REQUIRES(!stack_empty(S));
                c0_value y = pop(S);

                //takes care of zero division error case
                if (INT(y) == 0)
                {
                    c0_division_error("DIVISION BY 0 ERROR");
                    abort();
                }
                c0_value x = pop(S);
                //case of division error
                if (INT(x) == INT_MIN && INT(y) == -1)
                {
                    c0_division_error("DIV ERROR");
                    abort();
                }
                int vrem = INT(x) % INT(y);
                push(S, VAL(vrem));
                pc++;
                ENSURES(!stack_empty(S));
                break;
            }

            //bit and x & y
            case IAND:
            {
                REQUIRES(!stack_empty(S));
                c0_value y = pop(S);
                c0_value x = pop(S);
                int vand = INT(x) & INT(y);
                push(S, VAL(vand));
                pc++;
                ENSURES(!stack_empty(S));
                break;
            }

            //bit or x | y 
            case IOR:
            {
                REQUIRES(!stack_empty(S));
                c0_value y = pop(S);
                c0_value x = pop(S);
                int vor = INT(x) | INT(y);
                push(S, VAL(vor));
                pc++;
                ENSURES(!stack_empty(S));
                break;
            }

            //bit xor x ^ y
            case IXOR:
            {
                REQUIRES(!stack_empty(S));
                c0_value y = pop(S);
                c0_value x = pop(S);
                int vxor = INT(x) ^ INT(y);
                push(S, VAL(vxor));
                pc++;
                ENSURES(!stack_empty(S));
                break;
            }

            //bitshift left x << y
            case ISHL:
            {
                REQUIRES(!stack_empty(S));
                c0_value y = pop(S);
                c0_value x = pop(S);
                int vshl = INT(x) << (INT(y) & 31);
                push(S, VAL(vshl));
                pc++;
                ENSURES(!stack_empty(S));
                break;
            }

            //bitshift right x >> y
            case ISHR:
            {   
                REQUIRES(!stack_empty(S));
                c0_value y = pop(S);
                c0_value x = pop(S);
                int vshr = INT(x) >> (INT(y) & 31);
                push(S, VAL(vshr));
                pc++;
                ENSURES(!stack_empty(S));
                break;
            }


            /* Pushing small constants */
            
            //push constants onto operand stack
            case BIPUSH:
            {
                ubyte store = P[pc+1]; 
                push(S, VAL(INT(store))); 
                pc = pc + 2;
                ENSURES(!stack_empty(S));
                break;
            }


            /* Returning from a function */

            //return final value in operand stack
            case RETURN:
            {
                REQUIRES(!stack_empty(S));
                if(stack_size(S) == 1) //the return value is left in S
                {
                    c0_value ret_val = pop(S);
                    stack_free(S, NULL); free(V);
                    if(stack_empty(callStack)) //then just last function
                    {
                        return INT(ret_val);
                        free(callStack);
                    }
                    //when callStack isn't empty
                    else
                    {
                        frame next_frame = pop(callStack);
                        V = next_frame->V;
                        S = next_frame->S;
                        P = next_frame->P;
                        pc = next_frame->pc;
                        free(next_frame);
                        push(S, ret_val);
                    }      
                    break;
                }
            }


            /* Operations on local variables */

            //load variables onto operand stack
            case VLOAD:
            {
                push(S, V[P[pc+1]]);
                pc = pc + 2;
                ENSURES(!stack_empty(S));
                break;
            }

            //store variable 
            case VSTORE:
            {
                REQUIRES(!stack_empty(S));
                V[P[pc+1]] = pop(S);
                pc = pc + 2;
                break;
            }

            //calling NULL
            case ACONST_NULL:
            {
                push(S, NULL);
                pc++;
                ENSURES(!stack_empty(S));
                break;
            }

            //to store larger integer constants
            case ILDC:
            {
                ubyte c1 = P[pc+1];
                ubyte c2 = P[pc+2];
                int x = bc0->int_pool[((c1<<8) | c2)];
                push(S, VAL(x));
                pc = pc + 3;
                ENSURES(!stack_empty(S));
                break;
            }

            //to store strings
            case ALDC:
            {
                unsigned char c1 = P[pc+1];
                unsigned char c2 = P[pc+2];
                char* a = &(bc0->string_pool[((c1 << 8) | c2)]);
                push(S, VAL(a));
                pc = pc + 3;
                ENSURES(!stack_empty(S));
                break;
            }

            /* Control flow operations */

            //No operation
            case NOP:
            {
                pc++;
                break;
            }     

            // compares x == y
            case IF_CMPEQ:
            {  
                REQUIRES(!stack_empty(S));
                c0_value v1 = pop(S);
                c0_value v2 = pop(S);
                byte c1 = P[pc+1];
                byte c2 = P[pc+2];
                if (v1 == v2)
                    pc = pc + ((c1 << 8) | c2);
                //when doesn't pass then just move on normally
                else
                    pc = pc + 3; // v1 == v2 is FALSE
                break;
            }

            //compares x != y
            case IF_CMPNE:
            {
                REQUIRES(!stack_empty(S));
                c0_value v1 = pop(S);
                c0_value v2 = pop(S);
                byte c1 = P[pc+1];
                byte c2 = P[pc+2];
                if(v1 != v2)
                    pc = pc + ((c1 << 8) | c2);
                //when doesn't pass move normally
                else 
                    pc = pc + 3; // v1 != v2 is FALSE
                break;
            }

            //compares x < y     
            case IF_ICMPLT:
            {
                REQUIRES(!stack_empty(S));
                c0_value v1 = pop(S);
                c0_value v2 = pop(S);
                int x = INT(v2);
                int y = INT(v1);
                byte c1 = P[pc+1];
                byte c2 = P[pc+2];
                if(x < y)
                    pc = pc + ((c1 << 8) | c2);
                else
                    pc = pc + 3; // x < y is FALSE
                break;
            }
    
            //compares x >= y
            case IF_ICMPGE:
            {
                REQUIRES(!stack_empty(S));
                c0_value v1 = pop(S);
                c0_value v2 = pop(S);
                int x = INT(v2);
                int y = INT(v1);
                byte c1 = P[pc+1];
                byte c2 = P[pc+2];
                if(x >= y)
                    pc = pc + ((c1 << 8) | c2);
                else
                    pc = pc + 3; // x >= y is FALSE
                break;
            }

            //compares x > y
            case IF_ICMPGT:
            {
                REQUIRES(!stack_empty(S));
                c0_value v1 = pop(S);
                c0_value v2 = pop(S);
                int x = INT(v2);
                int y = INT(v1);
                byte c1 = P[pc+1];
                byte c2 = P[pc+2];
                if(x > y)
                    pc = pc + ((c1 << 8) | c2);
                else
                    pc = pc + 3; // x > y is FALSE
                break;
            }       

            //compares x <= y
            case IF_ICMPLE:
            {
                REQUIRES(!stack_empty(S));
                c0_value v1 = pop(S);
                c0_value v2 = pop(S);
                int x = INT(v2);
                int y = INT(v1);
                byte c1 = P[pc+1];
                byte c2 = P[pc+2]; 
                if(x <= y)
                    pc = pc + ((c1 << 8)| c2);
                else
                    pc = pc + 3; // x <= y is FALSE
                break;
            }

            //go to given pc
            case GOTO:
            {
                byte c1 = (P[pc+1]);
                byte c2 = (P[pc+2]);
                pc = (pc + ((c1 << 8) | c2));
                break;
            }


            /* Function call operations: */

            //calling other functions
            case INVOKESTATIC:
            {
                byte c1 = P[pc+1];
                byte c2 = P[pc+2];
                int fi_vars = bc0->function_pool[(c1<<8)|c2].num_vars;
                
                frame new_f = xmalloc(sizeof(struct frame));
                new_f->V = V;
                new_f->S = S;
                new_f->P = P;
                new_f->pc = pc+3;
                push(callStack, new_f);
                c0_value* new_V = (c0_value*)xcalloc(fi_vars, sizeof(c0_value));
                int fi_args = bc0->function_pool[(c1<<8)|c2].num_args;
                //grab arguments to give to functions
                for (int i = fi_args-1; i >= 0; i--)
                {
                    if(!stack_empty(S))
                        new_V[i] = pop(S);
                }
                V = new_V;
                P = bc0->function_pool[(c1<<8)|c2].code;
                S = stack_new();
                pc = 0;
                break;
            }
            
            //calling native function
            case INVOKENATIVE:
            {
                ubyte c1 = P[pc+1];
                ubyte c2 = P[pc+2];
                int args = bc0->native_pool[(c1<<8)|c2].num_args;
                
                c0_value* arr = (c0_value*)xcalloc(args, sizeof(c0_value));
                //grab args to give to native fn
                for (int i = args-1; i >= 0; i--)
                {   
                    if(!stack_empty(S))
                        arr[i] = pop(S);
                }
                int fn_index = bc0->native_pool[(c1<<8)|c2].function_table_index;
                push(S, native_function_table[fn_index](arr));
                pc = pc + 3;
                ENSURES(!stack_empty(S));
                break;
            }


            /* Memory allocation operations: */

            //allocate memory for var
            case NEW:
            {
                ubyte size = P[pc+1];
                c0_value* A = xmalloc(size);
                push(S, A);
                pc = pc + 2;
                ENSURES(!stack_empty(S));
                break;
            }
            
            //allocate memory for array
            case NEWARRAY:
            {
                REQUIRES(!stack_empty(S));
                ubyte size = P[pc+1];
                int n = INT(pop(S));
                struct c0_array *A = (struct c0_array*)xmalloc(8+n*size);
                A->count = n;
                A->elt_size = size;
                push(S, VAL(A));
                pc = pc + 2;
                ENSURES(!stack_empty(S));
                break;
            }   

            //get length of array
            case ARRAYLENGTH:
            {
                REQUIRES(!stack_empty(S));
                struct c0_array* arr = (struct c0_array*)pop(S);
                push(S,VAL(INT(arr->count)));
                pc++;
                ENSURES(!stack_empty(S));
                break;
            }


            /* Memory access operations: */

            //move address by f
            case AADDF:
            {
                REQUIRES(!stack_empty(S));
                ubyte f = P[pc+1];
                struct c0_array* A = (struct c0_array*)pop(S); 
                if (A != NULL)
                    push(S, VAL(INT(A)+8+f));   
                else  // if A is NULL ABORT
                {
                    c0_memory_error("ADDRESS DOESN'T EXIST"); 
                    abort();
                }
                pc = pc + 2;
                ENSURES(!stack_empty(S));
                break;
            }

            //get address from index in array
            case AADDS:
            {
                REQUIRES(!stack_empty(S));  
                int i = INT(pop(S));
                struct c0_array* A = (struct c0_array*)pop(S);
                int s = A->elt_size;

                if (A != NULL || (0 <= i && i < A->count))
                    push(S, VAL((INT(A)+(s*i))+8));
                else // if A is NULL ABORT
                {
                    c0_memory_error("ADDRESS DOESN'T EXIST");
                    abort();
                }
                pc++;
                ENSURES(!stack_empty(S));
                break;
            }

            //load 4 bytes x = a*
            case IMLOAD:
            {
                REQUIRES(!stack_empty(S));
                int* a = pop(S); 
               
                if (a != NULL)
                {
                    int load = *a;
                    push(S, VAL(load));
                }
                else
                {
                    c0_memory_error("ERROR");
                    abort();
                }
                pc++;
                ENSURES(!stack_empty(S));
                break;
            }

            //store 4 bytes
            case IMSTORE:
            {
                REQUIRES(!stack_empty(S));
                int store = INT(pop(S));
                int* a = pop(S);
                if (a != NULL)
                    *a = store;
                else
                {
                    c0_memory_error("ERROR");
                    abort();
                }
                pc++;
                break;
            
            }

            //load address
    	    case AMLOAD:
            {
                REQUIRES(!stack_empty(S));
                int* A = pop(S);
                if (A != NULL)
                {
                    int B = *A;
                    push(S, VAL(B));
                }
                else
                {
                    c0_memory_error("ERROR");
                    abort();
                }
                pc++;
                ENSURES(!stack_empty(S));
                break;
            }

            //store address
	        case AMSTORE:
            {
                REQUIRES(!stack_empty(S));
                int* B = pop(S);
                int* A = pop(S);
                if (A != NULL)
                    *A = INT(B);
                else
                {
                    c0_memory_error("ERROR");
                    abort();
                }
                pc++;
                break;
            }

            //load 1 byte
            case CMLOAD:
            {
                REQUIRES(!stack_empty(S));
                char* A = pop(S);
                if (A != NULL)
                {   
                    int store = (int)(*A);
                    push(S, VAL(store));
                }
                else
                {
                    c0_memory_error("ERROR");
                    abort();
                }
                pc++;
                ENSURES(!stack_empty(S));
                break;

            }

            //store 1 byte
            case CMSTORE:
            {
                REQUIRES(!stack_empty(S));
                int x = INT(pop(S));
                char* A = pop(S);
                if (A != NULL)
                    *A = (x & 0x7F);
                else // 
                {
                    c0_memory_error("ERROR");
                    abort();
                }
                pc++;
                break;
            }


                fprintf(stderr, "opcode not implemented: 0x%02x\n", P[pc]);
                abort();

            default:
                fprintf(stderr, "invalid opcode: 0x%02x\n", P[pc]);
                abort();
        }
    }

    /* cannot get here from infinite loop */
    assert(false);
}

