Linux Commands 

man : "manual", provides in depth information about the requested command
cp : "copy", copy files
mv : "move", move files
cd : "change directory", change directory
ln : "links", make links
mkdir : "make directory", make directories
cat : "concatenate", concatenate and print files
tac : does the reverse of cat
wc : "word count", word, line, character, and byte count
head : display first lines of a file
tail : display the last part of a file
echo : write arguments to the standard output
less : opposite of more (which displays text one screen at a time)
sort : sort lines of text files 
grep : print lines matching a pattern
find : walk a file hierarchy
ssh : OpenSSH SSH client (remote login program)
scp : secure copy (remote file copy program)
chmod : change file modes or Access Control Lists
chown : change file owner and group

Redirect : a commands output is sent to an alternate output

        command1  o   file1
        o---> where >, >>, <, >&

> : the command1 output is sent to file1 output, will always overwrite
>> : same as ">" only that it appends output to whatever was already in file1
< : executes command1 using files1 as source of input
>& : the standard error is redirected to standard output

Piping : the output of one program is sent as input to another program
       command1 | command2

| : executes command1 and use its output as the input for command2 

Exercise 4: 

In the command prompt, this person is trying to execute a file that isn't executable. Only
executable files (which appear green) can be executed using "./" . In order to make foo a readable and executable file then type the following in the command line:

$ chmod 755 foo
OR 
$ chmod u+x foo

Exercise 5: 

tac test.txt | tail | tac

Exercise 6:

find . -name "*.log" -exec cat {} \; | sort -k 6,4n -k 3,2n -M > $PROGRAM_PATH/master.log 

Exercise 7: 

find -type f -exec cat {} \; | grep -a 'WALNUT SPICE'




