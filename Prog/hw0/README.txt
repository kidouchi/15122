The function fastpow finds x^y or b^e. X is the base and y is the exponent.

About the loop variants:
It makes sure that the exponent(y) is positive or equal to 0. 
The integer r that is multiplied by b^e should equal x^y.
It basically checks that the result from the fastpow function equals to the pow
function. It makes sure that faspow works like x^y.


