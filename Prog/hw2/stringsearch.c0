#use <string>

/* some convenience functions wrapping string_compare */
bool string_leq(string s, string t) {
    return string_compare(s, t) <= 0;
}

bool string_lt(string s, string t) {
    return string_compare(s, t) < 0;
}

bool string_gt(string s, string t) {
    return string_compare(s, t) > 0;
}

/* code from lecture, ported to string arrays */
bool is_in(string x, string[] A, int n)
//@requires 0 <= n && n <= \length(A);
{
  int i;
  for (i = 0; i < n; i++)
    //@loop_invariant 0 <= i && i <= n;
    if (string_equal(A[i], x)) return true;
  return false;
}

bool is_sorted(string[] A, int lower, int upper)
//@requires 0 <= lower && lower <= upper && upper <= \length(A);
{ int i;
  for (i = lower; i < upper-1; i++)
    //@loop_invariant lower == upper || (lower <= i && i <= upper-1);
    if (!string_leq(A[i], A[i+1])) return false;
  return true;
}

int find(string x, string[] A, int n)
//@requires 0 <= n && n <= \length(A);
//@requires is_sorted(A,0,n);
{ int i;
  for (i = 0; i < n && string_leq(A[i], x); i++)
    //@loop_invariant 0 <= i && i <= n;
    //@loop_invariant i == 0 || string_lt(A[i-1], x);
    if (string_equal(A[i], x)) return i;
  return -1;
}

int binsearch(string x, string[] A, int n)
//@requires 0 <= n && n <= \length(A);
//@requires is_sorted(A,0,n);
//@ensures (\result == -1 && !is_in(x, A, n)) || string_equal(A[\result], x);
{ int lower = 0;
  int upper = n;
  while (lower < upper)
    //@loop_invariant 0 <= lower && lower <= upper && upper <= n;
    //@loop_invariant (lower == 0 || string_lt(A[lower-1], x));
    //@loop_invariant (upper == n || string_gt(A[upper], x));
    { int mid = lower + (upper-lower)/2; // (lower + upper)/2 could overflow
      int cmp = string_compare(A[mid], x);
      if (cmp == 0) return mid;
      else if (cmp < 0) lower = mid+1;
      else /*@assert(cmp > 0);@*/ upper = mid;
    }
  return -1;
}
