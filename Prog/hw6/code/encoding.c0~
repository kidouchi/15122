/******************************************************************************
 *                    15-122 Principles of Imperative Computation, Fall 2011
 ******************************************************************************
 *   This implements the Huffman encoding  algorithm
 *
 * @author
 ******************************************************************************/
#use <conio>
#use <string>
#use "bitstring.c0"
#use "heaps.c0"
#use "hash-maps.c0"

 /*********************************************************************
              Interface
 **********************************************************************/

/* Task 1. */
// Returns true if a tree is a Huffman treef; false - otherwise
bool is_htree(htree H);

// Returns true if a node is a leaf; false - otherwise
bool is_htree_leaf(htree H)
//@requires H != NULL;
;

// Returns true if a node is an internal node; false - otherwise
bool is_htree_node(htree H)
//@requires H != NULL;
;



/* Task 2. */
// Returns the total number of nodes in the Huffman tree H
int htree_size(htree H)
//@requires is_htree(H);
;

// Returns the number of leaves in the Huffman tree H
int htree_count_leaves(htree H)
//@requires is_htree(H);
;


/* Task 3. */
// Constructs an optimal encoding using Huffman's algorithm.
htree build_htree(char[] chars, int[] freqs, int n)
//@requires 1 < n && n <= \length(chars) && \length(chars) == \length(freqs);
//@ensures is_htree(\result);
//@ensures htree_count_leaves(\result) == n;
;




/* Task 4. */
//Creates a map: char->bitstring from the given Huffman tree H
hmap htree_to_map(htree H)
//@requires is_htree(H);
//@ensures is_hmap(\result);
;


/* Task 5. */
// Encodes the input string using the Huffman tree H and the above mapping
bitstring encode (htree H, string input)
//@requires is_htree(H);
//@ensures is_bitstring(\result);
;

 /*********************************************************************
              Your implementation
 **********************************************************************/

