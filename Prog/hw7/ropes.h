#ifndef ROPES_H_
#define ROPES_H_

#include <stdlib.h> /* for size_t */

/* The rope struct is hidden behind the interface below. */
typedef struct rope* rope;

/* Creates a rope from a NULL-terminated array of characters. */
rope rope_new(char* str);

/* Releases a reference to the rope.  If it was the last reference,
   frees the rope's heap memory and releases the references to any
   child ropes.
 */
void rope_release(rope str);

/* Returns the length of the rope str. */
size_t rope_length(rope str);

/* Creates a new rope representing the concatenation of str1 and str2. */
rope rope_join(rope str1, rope str2);

/* Retrieves the nth character of the rope str. */
char rope_charat(rope str, size_t n);

/* Returns 1 if str1 ">" str2.
   Returns 0 if str1 "=" str2.
   Returns -1 if str1 "<" str2.
*/
int rope_compare(rope str1, rope str2);

/* Converts the rope to a NUL-terminated char array. */
char* rope_to_chararray(rope str);

#endif
