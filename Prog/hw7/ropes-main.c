#include "ropes.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>


int main()
{
    rope new1 = rope_new("hello");
    rope new2 = rope_new("goodbye");
    char* carr1 = rope_to_chararray(new1);
    char* carr2 = rope_to_chararray(new2);

    printf("%s\n", "EXPECT: hello");
    printf("%s\n", carr1);
    printf("%s\n", "EXPECT: goodbye");
    printf("%s\n", carr2);

    rope new3 = rope_join(new1, new2);
    printf("%s\n", "EXPECT: hellogoodbye");
    char* carr3 = rope_to_chararray(new3);
    
    printf("%s\n", carr3);
        
    //should be 5
    rope_length(new1);
    rope_length(new2);

    rope_charat(new1, 4);
    rope_charat(new1, 1);
    rope_charat(new1, 0);
    //should cause error
    rope_charat(new1, 5);
    
    printf("%s\n", "EXPECT: -1");
    int check1 = rope_compare(new1, new2);
    printf("%d\n", check1);
    
    printf("%s\n", "EXPECT: 1");
    int check2 = rope_compare(new2, new1);
    printf("%d\n", check2);

    printf("%s\n", "EXPECT: 1");
    int check3 = rope_compare(new3, new1);
    printf("%d\n", check3);

    printf("%s\n", "EXPECT: -1");
    int check4 = rope_compare(new1, new3);
    printf("%d\n", check4);

    rope new4 = rope_new("filos");
    printf("%s\n", "EXPECT: 0");
    int check5 = rope_compare(new1, new4);
    printf("%d\n", check5);

    rope_release(new1);
    rope_release(new2);
    rope_release(new3);
    rope_release(new4);
    free(carr1);
    free(carr2);
    free(carr3);
    return 0;
    /*
    free(check1);
    free(check2);
    free(check3);
    free(check4);
    free(check5);*/
}