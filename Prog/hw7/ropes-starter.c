#include "ropes.h"
#include <string.h>
#include <assert.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include "xalloc.h"
#define REQUIRES(COND) assert(COND)
#define ENSURES(COND) assert(COND)


struct rope 
{
  size_t size;          /* size of the string in this rope */
  size_t position;      /* length of the left rope */
  struct rope* left;    /* pointer to left child, NULL for leaves */
  struct rope* right;   /* pointer to right child, NULL for leaves */
  char* data;           /* string data, NULL for interior nodes */
  int ref_count;        /* number of references to this rope */
};

/* TODO: implement functions here (Tasks 1-8) */

//check is valid rope
bool is_rope(rope s)
{
    if (s == NULL)
        return false;
    
    //check if it's a leaf
    if (s->left == NULL && s->right == NULL)
    {
        if (s->data == NULL)
            return false;
        if (s->size != strlen(s->data))
            return false;
        if (s->position != 0)
            return false;
        if (s->ref_count < 1)
            return false;
    }
    
    //check if it's an interior node
    else 
    {
        if (s->data != NULL)
            return false;
        if (s->left == NULL || s->right == NULL)
            return false;
        if (!(is_rope(s->left) && is_rope(s->right)))
            return false;
        if (s->size != (s->left->size + s->right->size))
            return false;
        if (s->left->size != s->position)
            return false;
        if (s->ref_count < 1)
            return false;
    }
    return true;
}

//find length of rope
size_t rope_length(rope str)
{
    REQUIRES(is_rope(str));
    return strlen(str->data);
}

//make deep copy of rope
rope rope_new(char* str)
{
    rope new = xmalloc(sizeof(struct rope));
    //check case where str may be null
    if(str==NULL)
        new->size = 0;
    else
        new->size = strlen(str);
    new->position = 0;
    new->data = str;
    new->ref_count = 1;
    new->left = NULL; new->right = NULL;
    ENSURES(is_rope(new));    
    return new;
}

//join ropes str1 and str2
rope rope_join(rope str1, rope str2)
{
    REQUIRES(is_rope(str1) && is_rope(str2));
    rope new = xmalloc(sizeof(struct rope));
    new->size = str1->size + str2->size;
    new->position = str1->size;
    new->left = str1;
    new->right = str2;
    new->ref_count = 1;
    str1->ref_count++; str2->ref_count++;
    ENSURES(is_rope(new));
    return new;
}

//release rope with no more references
void rope_release(rope str)
{
    REQUIRES(is_rope(str));
    
    //decrement reference in rope
    str->ref_count = str->ref_count - 1; 
    
    //check case where at leaf
    //free rope str
    if (str->left == NULL && str->right == NULL)
    {
        if (str->ref_count == 0)
            free(str);
    }
    else
    {
        if (str->ref_count == 0)
        {
            rope_release(str->left);
            rope_release(str->right);
            //if ref count is 0
            //then free rope str
            free(str);
        }
    }
}
    
//get char at index i in str    
char rope_charat(rope str, size_t i)
{
    REQUIRES(is_rope(str));
    
    //Base Case
    //if at leaf then get char
    if (str->left == NULL && str->right == NULL)
        return str->data[i];
    //if char is on right of rope
    if (i >= str->position)
        return rope_charat(str->right, i-(str->position));
    //if char is on left
    return rope_charat(str->left, i);

}

//compare str1 and str2 if less, greater, or equal
int rope_compare(rope str1, rope str2)
{
    REQUIRES(is_rope(str1) && is_rope(str2));
    
    char* check1 = rope_to_chararray(str1);
    char* check2 = rope_to_chararray(str2);
    int compare = strcmp(check1, check2); 
    free(check1); free(check2);
    if (compare < 0)
        return -1;
    //if str1 greater than str2
    else if (compare > 0)
        return 1;
    //if str1 equal str2      
    return 0;
}

//get full string from rope str
char* rope_to_chararray(rope str)
{
    REQUIRES(is_rope(str));
    char* finalstr = (char*)xcalloc((str->size)+1, sizeof(char));
    
    //get each char in rope str
    for (unsigned int i = 0; i < str->size; i++)
        finalstr[i] = rope_charat(str, i);
    finalstr[str->size] = '\0';
    return finalstr;
}



