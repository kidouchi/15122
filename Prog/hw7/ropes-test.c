
/*
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include "ropes-starter.c"

int main(){

///////// set up Minnesota
    printf("0 HEY GAAAAAAAAIIIIIS. ");
    printf("1 init Min, Ne, Sota ");
    rope Min = rope_new("Min");
    rope Ne = rope_new("Ne");
    rope Sota = rope_new("Sota");
    printf("2 joining Min, Ne");
    
    
    rope Minne = rope_join(Min, Ne);
    printf("3 joined Minne, joining Sota");
    printf("hi");
    
    rope Minnesota = rope_new("");
    Minnesota = rope_join(Minne, Sota);
    printf("4 joined Minnesota");
    printf("data in Minnesota by concat_message");
    printf("message in Min is %s", Min->data);
    printf("message in Ne is %s", Ne->data);
    printf("Sota says %s", Sota->data);
    printf("Minne says %s", concat_message(Minne));
    printf("Minnesota says %s", concat_message(Minnesota)); 
 
return 0;
}
*/
#include "ropes.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>


int main()
{
    rope hello = rope_new("hello");
    rope new2 = rope_new("goodbye");
    char* carr1 = rope_to_chararray(hello);
    char* carr2 = rope_to_chararray(new2);

    printf("%s\n", "EXPECT: hello");
    printf("%s\n", carr1);
    printf("%s\n", "EXPECT: goodbye");
    printf("%s\n", carr2);

    rope new3 = rope_join(hello, new2);
    printf("%s\n", "EXPECT: hellogoodbye");
    char* carr3 = rope_to_chararray(new3);
    
    printf("%s\n", carr3);
        
    //should be 5
    rope_length(hello);
    rope_length(new2);

    printf("GOING INTO CHARAT!!");
    rope_charat(hello, 4);
    printf("PASSED 1I?");
    rope_charat(hello, 1);
    printf("PASSED 2?");
    rope_charat(hello, 0);
    printf("PASSED 3?");
    //should cause error
    //rope_charat(hello, 5);
    //printf("SHOULD CAUSE ERROR!");
    
    printf("%s\n", "EXPECT: -1");
    int check1 = rope_compare(hello, new2);
    printf("%d\n", check1);
    
    printf("%s\n", "EXPECT: 1");
    int check2 = rope_compare(new2, hello);
    printf("%d\n", check2);

    printf("%s\n", "EXPECT: 1");
    int check3 = rope_compare(new3, hello);
    printf("%d\n", check3);

    printf("%s\n", "EXPECT: -1");
    int check4 = rope_compare(hello, new3);
    printf("%d\n", check4);

    rope new4 = rope_new("filos");
    printf("%s\n", "EXPECT: 0");
    int check5 = rope_compare(hello, new4);
    printf("%d\n", check5);

    rope_release(hello);
    rope_release(new2);
    rope_release(new3);
    rope_release(new4); ///////////
    free((void *)carr1);
    free((void *)carr2);
    free((void *)carr3);
    return 0;

}


